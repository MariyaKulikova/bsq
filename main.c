/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osook <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 13:09:12 by osook             #+#    #+#             */
/*   Updated: 2020/08/05 23:05:46 by mnahasap         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

char		*ft_strcat(char *s1, char *s2)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (s1[i])
		i++;
	while (s2[j])
	{
		s1[i] = s2[j];
		j++;
		i++;
	}
	s1[i] = '\0';
	return (s1);
}

void		read_stdin(void)
{
	char	buf[SIZE];
	int		ret;
	char	file[SIZE];
	int		ultra_ret;

	file[0] = '\0';
	ultra_ret = 0;
	while ((ret = read(0, buf, SIZE)) > 0)
	{
		ultra_ret += ret;
		buf[ret] = '\0';
		ft_strcat(file, buf);
	}
	process_file(file, ultra_ret);
}

int			main(int argc, char *argv[])
{
	int		j;
	int		fd;
	char	buf[SIZE];
	int		ret;

	if (argc > 1)
	{
		j = 1;
		while (j < argc)
		{
			fd = open(&argv[j][0], O_RDONLY);
			j++;
			if (fd == -1)
			{
				write(2, MAP_ERROR, MAP_ERROR_SIZE);
				continue ;
			}
			while ((ret = read(fd, buf, SIZE)) > 0)
				process_file(buf, ret);
			close(fd);
		}
	}
	else
		read_stdin();
	return (0);
}
