/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_square.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osook <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/05 16:58:32 by osook             #+#    #+#             */
/*   Updated: 2020/08/05 23:01:26 by mnahasap         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "process_file.h"
#include <stdio.h>

void		set_coord(t_coord **coord, int **digit_map, int i, int j)
{
	if (digit_map[i][j] > (*coord)->max_value)
	{
		(*coord)->max_value = digit_map[i][j];
		(*coord)->i_finish = i;
		(*coord)->i_start = i - ((*coord)->max_value - 1);
		(*coord)->j_finish = j;
		(*coord)->j_start = j - ((*coord)->max_value - 1);
	}
}

t_coord		*find_coord(int **digit_map, t_map_param *param)
{
	t_coord *coord;
	int		i;
	int		j;

	coord = (t_coord *)malloc(sizeof(t_coord));
	i = 0;
	j = 0;
	coord->max_value = digit_map[i][j];
	while (i < param->y)
	{
		while (j < param->x)
		{
			set_coord(&coord, digit_map, i, j);
			j++;
		}
		j = 0;
		i++;
	}
	return (coord);
}

void		draw_square(char **map, int **digit_map, t_map_param *param)
{
	t_coord *coord;
	int		i;
	int		j;

	coord = find_coord(digit_map, param);
	i = coord->i_start;
	j = coord->j_start;
	while (i <= coord->i_finish)
	{
		while (j <= coord->j_finish)
		{
			map[i][j] = param->full;
			j++;
		}
		j = coord->j_start;
		i++;
	}
}
