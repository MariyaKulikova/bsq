/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copy_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osook <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/04 19:11:43 by osook             #+#    #+#             */
/*   Updated: 2020/08/05 22:29:45 by mnahasap         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "process_file.h"
#include <unistd.h>

int			set_digit(int **map, int *digit, int i, int j)
{
	int		x;
	int		y;
	int		min;

	x = 0;
	y = 0;
	min = map[i + x][j + y];
	while ((x < 2) && (y = 0))
	{
		while (y < 2)
		{
			if (x == 1 && y == 1)
				break ;
			if (x == 1 && y == 0)
			{
				if (digit[j + y] < min)
					min = digit[j + y];
			}
			else if (map[i + x][j + y] < min)
				min = map[i + x][j + y];
			y++;
		}
		x++;
	}
	return (min + 1);
}

int			**copy_map(char **map, t_map_param *param)
{
	int		i;
	int		j;
	int		**copy_map;
	int		*digit;

	copy_map = (int **)malloc(param->y * sizeof(int *));
	i = 0;
	j = 0;
	while (i < param->y)
	{
		digit = (int *)malloc(param->x * sizeof(int));
		while (j < param->x)
		{
			if (((i == 0) || (j == 0)) && (map[i][j] == param->empty))
				digit[j] = 1;
			else if (map[i][j] == param->obstacle)
				digit[j] = 0;
			else if (i != 0 || j != 0)
				digit[j] = set_digit(copy_map, digit, (i - 1), (j - 1));
			j++;
		}
		copy_map[i++] = digit;
		j = 0;
	}
	return (copy_map);
}
