NAME = BSQ

SRC = copy_map.c process_file.c main.c draw_square.c ultimate_free.c

OBJ = $(SRC:.c=.o)

CC = gcc

CFLAGS = -Wall -Wextra -Werror

.PHONY:		clean

all: 		$(NAME)

.c.o:
		$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)

$(NAME): 	$(OBJ)
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

clean:		
		rm -f $(OBJ)

fclean: 	clean
		rm -f $(NAME)

re:		fclean all


