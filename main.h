/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osook <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 13:20:13 by osook             #+#    #+#             */
/*   Updated: 2020/08/04 12:58:38 by osook            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include <unistd.h>
# include <fcntl.h>
# define MAP_ERROR "map error\n"
# define MAP_ERROR_SIZE 9
# define SIZE 524288

void	process_file(char *buf, int ret);

#endif
