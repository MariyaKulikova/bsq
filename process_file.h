/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_file.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osook <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 14:27:57 by osook             #+#    #+#             */
/*   Updated: 2020/08/04 14:36:15 by osook            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROCESS_FILE_H
# define PROCESS_FILE_H

# include <stdlib.h>
# include <unistd.h>
# define SIZE 524288
typedef struct	s_map_param
{
	int			x;
	int			y;
	int			str_len;
	int			f_str_len;
	char		empty;
	char		obstacle;
	char		full;
}				t_map_param;

typedef struct 	s_coord
{
	int			i_start;
	int			i_finish;
	int			j_start;
	int			j_finish;
	int			max_value;
}				t_coord;

int             **copy_map(char **map, t_map_param *param);
void			draw_square(char **map, int **digit_map, t_map_param *param);
void    		ultimate_free(void **tab, int y);
#endif
