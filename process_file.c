/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osook <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/03 14:26:09 by osook             #+#    #+#             */
/*   Updated: 2020/08/04 18:25:08 by osook            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "process_file.h"
#include <stdio.h>

void			set_str_count(char *buf, t_map_param *param, int j)
{
	int i;
	
	i = 0;
	while (i < j)
	{
		if (buf[i] >= '0' && buf[i] <= '9')
			param->y = (10 * param->y) + buf[i] - '0';
		i++;
	}
}

t_map_param		*set_param(char *buf, int ret)
{
	t_map_param *param;
	int			i;

	param = (t_map_param *)malloc(sizeof(t_map_param));
	param->y = 0;
	param->x = 0;
	i = 0;
	while (buf[i] != '\n' && i < ret)
		i++;
	param->f_str_len = i;
	param->full = buf[i - 1];
	param->obstacle = buf[i - 2];
	param->empty = buf[i - 3];
	set_str_count(buf, param, (i - 3));
	i++;
	while (buf[i] != '\n' && i < ret)
	{
		param->x += 1;
		i++;
	}
	return (param);
}

void			print_map(char **map, t_map_param *param)
{
	int			i;
	int			j;

	i = 0;
	j = 0;
	while (i < param->y)
	{
		while (j < param->x)
		{
			write(1, &map[i][j], 1);
			j++;
		}
		write(1, "\n", 1);
		j = 0;
		i++;
	}
}

char			**create_map(char *buf, int ret, t_map_param *param)
{
	int			i;
	int			j;
	int			k;
	char		**map;
	char		*str;

	map = (char **)malloc(param->y * sizeof(char *));
	i = param->f_str_len + 1;
	j = 0;
	while ((j < param->y) && (i < ret))
	{
		str = (char *)malloc(param->x * sizeof(char));
		k = 0;
		while (k < param->x)
		{
			if (buf[i] != '\n')
				str[k++] = buf[i++];
			else
				i++;
		}
		map[j++] = str;
	}
	return (map);
}

void			process_file(char *buf, int ret)
{
	t_map_param *param;
	char		**map;
    int         **digit_map;

	param = set_param(buf, ret);
	map = create_map(buf, ret, param);
    digit_map = copy_map(map, param);
	draw_square(map, digit_map, param);
	print_map(map, param);
	ultimate_free((void **)map, param->y);
	ultimate_free((void **)digit_map, param->y);
	free(param);
}
